#!/usr/bin/env python3.3

NumberOfPeriod = 3

def go():
	# Input
	num = int(input("Numerator: "))
	denom = int(input("Denominator: ")) #fix division with 0
	
	# Heltals division
	quotient = num//denom
	remainder = [num%denom]

	# Rakna ut decimalerna
	if remainder[0] != 0:
		remainder, decimals = deci(remainder, denom)
	else:
		decimals = [0]
	
	# utskrift, tva fall
	if remainder[-1] == 0:	
		print('Non periodic,', answer(quotient, decimals, num, denom))
	else:
		per = period(remainder, decimals)
		per = ''.join([str(x) for x in per])
		print('It\'s periodic,', answer(quotient, decimals, num, denom), 'where the period is', per)
	
def deci(remainder, denom):
	decimals = []
	while more_decimals(remainder):
		decimals.append(remainder[-1]*10//denom)
		remainder.append(remainder[-1]*10%denom)
	return remainder, decimals

def more_decimals(remainder):
	if remainder[-1] == 0:
		next_decimal = False
	elif remainder.count(remainder[-1]) == NumberOfPeriod:
		next_decimal = False
	else:
		next_decimal = True
	return next_decimal

def period(remainder,decimals):
	index = [i for i in range(len(remainder)) if remainder[i] == remainder[-1]]
	return decimals[index[0]:index[1]]
	
def answer(quotient, decimals, num, denom):
	ans = str(quotient) + ',' + ''.join([str(x) for x in decimals])
	return str(num) + ' / ' + str(denom) + ' = ' + ans

	


