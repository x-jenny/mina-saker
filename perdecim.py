#!/usr/bin/env python3.3


def go():
	# Input
	num = int(input("Numerator: "))
	denom = int(input("Denominator: ")) #fix division with 0
	
	# Heltals division
	quotient = num//denom
	remainder = [num%denom]

	# Rakna ut decimalerna
	if remainder[0] != 0:
		remainder, decimals = deci(remainder, denom)
	else:
		decimals = [0]
	
	# utskrift, tva fall
	if remainder[-1] == 0:	
		print('Non periodic,', answer(quotient, decimals, num, denom))
	else:
		per = period(remainder, decimals)
		per = ''.join([str(x) for x in per])
		print('It\'s periodic,', answer(quotient, decimals, num, denom), 'where the period is', per)
	
def deci(remainder, denom):
	decimals = []
	while remainder[-1] != 0 and remainder[-1] not in remainder[:-1]:
		decimals.append(remainder[-1]*10//denom)
		remainder.append(remainder[-1]*10%denom)
	return remainder, decimals

def period(remainder,decimals):
	pstart = remainder.index(remainder[-1])
	return decimals[pstart:]
	
def answer(quotient, decimals, num, denom):
	ans = str(quotient) + ',' + ''.join([str(x) for x in decimals])
	return str(num) + ' / ' + str(denom) + ' = ' + ans

	


